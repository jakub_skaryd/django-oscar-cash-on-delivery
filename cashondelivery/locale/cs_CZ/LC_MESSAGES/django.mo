��          �   %   �      p     q     x     �     �     �     �     �  	               	        '     ,     ;     O     b  #   i     �     �     �  	   �     �     �     �     	  -     &   D  �  k     �     �               +     A     X  
   r     }     �  
   �     �     �     �     �     �  0   �          &     5  	   F     P     _     {  	   �  #   �      �                                                                                                  	              
           Amount Billing address Cash on Delivery Cash on Delivery transaction Cash on Delivery transactions Change payment details Change payment method Confirmed Continue Currency Dashboard Date Date confirmed Enter a new address Invalid submission Method No transactions have been made yet. Order number Payment details Payment method Reference Reference hash Same as the shipping address Transaction %(reference)s Transactions What address should the invoice be issued to? You've chosen to pay cash on delivery. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-01 23:15+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Částka Fakturační adresa Dobírka Transakce dobírky Dobírkové transakce Změnte detaily platby Změňte platební metodu Potvrzené Pokračujte Měna Nástěnka Datum Datum potvrzení Zadejte novou adresu Nevalidní podání Metoda Ještě nebyly uskutečněné žádné transakce Číslo objednávky Detaily platby Platební metoda Reference Hash reference Stejná jako dodací adresa Transakce %(reference)s Transakce Na koho má být vystavena faktura? Vybrali jste si platbu dobírkou 