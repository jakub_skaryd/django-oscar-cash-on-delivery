from django.conf.urls import url
from oscar.core.application import Application
from oscar.core.loading import get_class


class CODApplication(Application):
    name = 'cashondelivery'

    payment_details_view = get_class('cashondelivery.views', 'PaymentDetailsView')

    def get_urls(self):
        urls = [
            url(r'payment-details/$',
                self.payment_details_view.as_view(), name='payment-details'),

            # Preview and thankyou
            url(r'preview/$',
                self.payment_details_view.as_view(preview=True),
                name='preview'),
        ]
        return self.post_process_urls(urls)

application = CODApplication()

